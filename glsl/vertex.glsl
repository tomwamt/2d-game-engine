#version 330 core

in vec2 vertexPosition;
in vec2 vertexUV;

out vec2 texCoords;

uniform mat4 proj;
uniform mat4 view;
uniform mat4 model;

void main(){
	vec4 pos = vec4(vertexPosition, 0, 1);
	gl_Position = proj * view * model * pos;
	
	texCoords = vertexUV;
}