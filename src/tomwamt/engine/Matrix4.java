package tomwamt.engine;
import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;

public class Matrix4 {
	// Column major
	private float[] data;
	private FloatBuffer buffer;
	
	public Matrix4(){
		data = new float[16];
		buffer = BufferUtils.createFloatBuffer(16);
		setIdentity();
	}
	
	public Matrix4 setZero(){
		for(int i = 0; i < 16; i++){
			data[i] = 0;
		}
		return this;
	}
	
	public Matrix4 setIdentity(){
		for(int i = 0; i < 4; i++){
			for(int j = 0; j < 4; j++){
				if(i == j)
					set(i, j, 1);
				else
					set(i, j, 0);
			}
		}
		return this;
	}
	
	public void set(int col, int row, float value){
		data[col*4+row] = value;
	}
	
	public float get(int col, int row){
		return data[col*4+row];
	}
	
	public static Matrix4 scale(float scaleX, float scaleY, float scaleZ){
		Matrix4 mat = new Matrix4();
		mat.set(0, 0, scaleX);
		mat.set(1, 1, scaleY);
		mat.set(2, 2, scaleZ);
		return mat;
	}
	
	public static Matrix4 rotate(float axisX, float axisY, float axisZ, float theta){
		float sin = (float) Math.sin(theta);
		float cos = (float) Math.cos(theta);
		
		float mag = (float) Math.sqrt(axisX*axisX + axisY*axisY + axisZ*axisZ);
		axisX /= mag;
		axisY /= mag;
		axisZ /= mag;
		
		Matrix4 mat = new Matrix4();
		mat.set(0, 0, axisX*axisX*(1-cos)+cos);
		mat.set(0, 1, axisY*axisX*(1-cos)-axisZ*sin);
		mat.set(0, 2, axisZ*axisX*(1-cos)+axisY*sin);
		mat.set(1, 0, axisX*axisY*(1-cos)+axisZ*sin);
		mat.set(1, 1, axisY*axisY*(1-cos)+cos);
		mat.set(1, 2, axisZ*axisY*(1-cos)-axisX*sin);
		mat.set(2, 0, axisX*axisZ*(1-cos)-axisY*sin);
		mat.set(2, 1, axisY*axisZ*(1-cos)+axisX*sin);
		mat.set(2, 2, axisZ*axisZ*(1-cos)+cos);
		
		return mat;
	}
	
	public static Matrix4 translate(float transX, float transY, float transZ){
		Matrix4 mat = new Matrix4();
		mat.set(0, 3, transX);
		mat.set(1, 3, transY);
		mat.set(2, 3, transZ);
		return mat;
	}
	
	public Matrix4 scalarMult(float scalar){
		for(int i = 0; i < 16; i++)
			data[i] *= scalar;
		return this;
	}
	
	public Matrix4 mult(Matrix4 other){
		// this is left, other is right
		float[] result = new float[16];
		for(int i = 0; i < 4; i++){
			for(int j = 0; j < 4; j++){
				result[i*4+j] = this.get(i, 0)*other.get(0, j) + this.get(i, 1)*other.get(1, j) + this.get(i, 2)*other.get(2, j) + this.get(i, 3)*other.get(3, j);
			}
		}
		
		data = result;
		return this;
	}
	
	public float[] getData(){
		return data;
	}
	
	public FloatBuffer asFloatBuffer(){
		buffer.clear();
		buffer.put(data);
		buffer.flip();
		return buffer;
	}
	
	@Override
	public String toString(){
		// Note: printing is transpose
		String s = String.format("[[%.2f %.2f %.2f %.2f]\n", get(0, 0), get(0, 1), get(0, 2), get(0, 3));
		s += String.format(" [%.2f %.2f %.2f %.2f]\n", get(1, 0), get(1, 1), get(1, 2), get(1, 3));
		s += String.format(" [%.2f %.2f %.2f %.2f]\n", get(2, 0), get(2, 1), get(2, 2), get(2, 3));
		s += String.format(" [%.2f %.2f %.2f %.2f]]", get(3, 0), get(3, 1), get(3, 2), get(3, 3));
		return s;
	}
}
