package tomwamt.engine;

public class Camera {
	private float x, y;
	private float rot;
	private float size; // Vertical size
	
	protected Camera(){
		x = 0;
		y = 0;
		rot = 0;
		size = 5;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getRot() {
		return rot;
	}

	public void setRot(float rot) {
		this.rot = rot % 360;
	}

	public float getSize() {
		return size;
	}

	public void setSize(float size) {
		if(size > 0)
			this.size = size;
	}
}
