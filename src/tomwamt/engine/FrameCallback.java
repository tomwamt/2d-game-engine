package tomwamt.engine;

public abstract class FrameCallback {
	private MainWindow window;
	
	public FrameCallback(MainWindow window){
		this.window = window;
	}
	
	public MainWindow getWindow(){
		return window;
	}
	
	public abstract void update(float delta);
}
