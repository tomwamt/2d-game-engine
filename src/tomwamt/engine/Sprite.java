package tomwamt.engine;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.GL_CLAMP_TO_EDGE;
import static org.lwjgl.opengl.GL30.*;

import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.FloatBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;

public class Sprite {
	private static final float degToRad = (float)Math.PI/180;
	
	private float posX, posY;
	private float rot;
	private float scaleX, scaleY;
	
	private int tex;
	
	public Sprite(String filename){this(0, 0, 0, 1, 1, filename);}
	public Sprite(float posX, float posY, String filename){this(posX, posY, 0, 1, 1, filename);}
	public Sprite(float posX, float posY, float rot, float scaleX, float scaleY, String filename){
		this.posX = posX;
		this.posY = posY;
		this.rot = rot;
		this.scaleX = scaleX;
		this.scaleY = scaleY;
		
		tex = loadTexture(filename);
	}
	
	public void setPosition(float x, float y){
		posX = x;
		posY = y;
	}
	
	public float getX(){
		return posX;
	}
	
	public float getY(){
		return posY;
	}
	
	public void setRotation(float rot){
		this.rot = rot % 360;
	}
	
	public float getRotation(){
		return rot;
	}
	
	public void setScale(float width, float height){
		scaleX = width;
		scaleY = height;
	}
	
	public float getWidth(){
		return scaleX;
	}
	
	public float getHeight(){
		return scaleY;
	}
	
	public void setTexture(String filename){
		if(tex != 0) glDeleteTextures(tex);
		tex = loadTexture(filename);
	}
	
	public int getTextureID(){
		return tex;
	}
	
	public boolean collidesWith(Sprite other){
		float cosMe = (float) Math.cos(rot * degToRad);
		float sinMe = (float) Math.sin(rot * degToRad);
		float cosOt = (float) Math.cos(other.rot * degToRad);
		float sinOt = (float) Math.sin(other.rot * degToRad);
		
		float xMe = scaleX/2;
		float yMe = scaleY/2;
		float xOt = other.scaleX/2;
		float yOt = other.scaleY/2;
		
		float trxMe = posX + xMe*cosMe - yMe*sinMe;
		float tryMe = posY + xMe*sinMe + yMe*cosMe; // try me punk
		float tlxMe = posX - xMe*cosMe - yMe*sinMe;
		float tlyMe = posY - xMe*sinMe + yMe*cosMe;
		float blxMe = posX - xMe*cosMe + yMe*sinMe;
		float blyMe = posY - xMe*sinMe - yMe*cosMe;
		float brxMe = posX + xMe*cosMe + yMe*sinMe;
		float bryMe = posY + xMe*sinMe - yMe*cosMe;
		
		float trxOt = other.posX + xOt*cosOt - yOt*sinOt;
		float tryOt = other.posY + xOt*sinOt + yOt*cosOt;
		float tlxOt = other.posX - xOt*cosOt - yOt*sinOt;
		float tlyOt = other.posY - xOt*sinOt + yOt*cosOt;
		float blxOt = other.posX - xOt*cosOt + yOt*sinOt;
		float blyOt = other.posY - xOt*sinOt - yOt*cosOt;
		float brxOt = other.posX + xOt*cosOt + yOt*sinOt;
		float bryOt = other.posY + xOt*sinOt - yOt*cosOt;
		
		boolean trb = sideOfLine(trxMe, tryMe, tlxMe, tlyMe, trxOt, tryOt) <= 0;
		boolean tlb = sideOfLine(trxMe, tryMe, tlxMe, tlyMe, tlxOt, tlyOt) <= 0;
		boolean blb = sideOfLine(trxMe, tryMe, tlxMe, tlyMe, blxOt, blyOt) <= 0;
		boolean brb = sideOfLine(trxMe, tryMe, tlxMe, tlyMe, brxOt, bryOt) <= 0;
		if(trb && tlb && blb && brb) return false;
		
		trb = sideOfLine(tlxMe, tlyMe, blxMe, blyMe, trxOt, tryOt) <= 0;
		tlb = sideOfLine(tlxMe, tlyMe, blxMe, blyMe, tlxOt, tlyOt) <= 0;
		blb = sideOfLine(tlxMe, tlyMe, blxMe, blyMe, blxOt, blyOt) <= 0;
		brb = sideOfLine(tlxMe, tlyMe, blxMe, blyMe, brxOt, bryOt) <= 0;
		if(trb && tlb && blb && brb) return false;
		
		trb = sideOfLine(blxMe, blyMe, brxMe, bryMe, trxOt, tryOt) <= 0;
		tlb = sideOfLine(blxMe, blyMe, brxMe, bryMe, tlxOt, tlyOt) <= 0;
		blb = sideOfLine(blxMe, blyMe, brxMe, bryMe, blxOt, blyOt) <= 0;
		brb = sideOfLine(blxMe, blyMe, brxMe, bryMe, brxOt, bryOt) <= 0;
		if(trb && tlb && blb && brb) return false;
		
		trb = sideOfLine(brxMe, bryMe, trxMe, tryMe, trxOt, tryOt) <= 0;
		tlb = sideOfLine(brxMe, bryMe, trxMe, tryMe, tlxOt, tlyOt) <= 0;
		blb = sideOfLine(brxMe, bryMe, trxMe, tryMe, blxOt, blyOt) <= 0;
		brb = sideOfLine(brxMe, bryMe, trxMe, tryMe, brxOt, bryOt) <= 0;
		if(trb && tlb && blb && brb) return false;
		
		trb = sideOfLine(trxOt, tryOt, tlxOt, tlyOt, trxMe, tryMe) <= 0;
		tlb = sideOfLine(trxOt, tryOt, tlxOt, tlyOt, tlxMe, tlyMe) <= 0;
		blb = sideOfLine(trxOt, tryOt, tlxOt, tlyOt, blxMe, blyMe) <= 0;
		brb = sideOfLine(trxOt, tryOt, tlxOt, tlyOt, brxMe, bryMe) <= 0;
		if(trb && tlb && blb && brb) return false;
		
		trb = sideOfLine(tlxOt, tlyOt, blxOt, blyOt, trxMe, tryMe) <= 0;
		tlb = sideOfLine(tlxOt, tlyOt, blxOt, blyOt, tlxMe, tlyMe) <= 0;
		blb = sideOfLine(tlxOt, tlyOt, blxOt, blyOt, blxMe, blyMe) <= 0;
		brb = sideOfLine(tlxOt, tlyOt, blxOt, blyOt, brxMe, bryMe) <= 0;
		if(trb && tlb && blb && brb) return false;
		
		trb = sideOfLine(blxOt, blyOt, brxOt, bryOt, trxMe, tryMe) <= 0;
		tlb = sideOfLine(blxOt, blyOt, brxOt, bryOt, tlxMe, tlyMe) <= 0;
		blb = sideOfLine(blxOt, blyOt, brxOt, bryOt, blxMe, blyMe) <= 0;
		brb = sideOfLine(blxOt, blyOt, brxOt, bryOt, brxMe, bryMe) <= 0;
		if(trb && tlb && blb && brb) return false;
		
		trb = sideOfLine(brxOt, bryOt, trxOt, tryOt, trxMe, tryMe) <= 0;
		tlb = sideOfLine(brxOt, bryOt, trxOt, tryOt, tlxMe, tlyMe) <= 0;
		blb = sideOfLine(brxOt, bryOt, trxOt, tryOt, blxMe, blyMe) <= 0;
		brb = sideOfLine(brxOt, bryOt, trxOt, tryOt, brxMe, bryMe) <= 0;
		if(trb && tlb && blb && brb) return false;
			
		return true;
	}
	
	private int loadTexture(String filename){
		try {
			BufferedImage img = ImageIO.read(new File(filename));
			int w = img.getWidth();
			int h = img.getHeight();
			
			int tex = glGenTextures();
			glBindTexture(GL_TEXTURE_2D, tex);
			
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			
			FloatBuffer buffer = BufferUtils.createFloatBuffer(w*h*4);
			for(int argb : img.getRGB(0, 0, w, h, null, 0, w)){
				int a = (argb >> 24) & 0xFF;
				int r = (argb >> 16) & 0xFF;
				int g = (argb >> 8)  & 0xFF;
				int b = argb & 0xFF;
				
				buffer.put(r/255.0f);
				buffer.put(g/255.0f);
				buffer.put(b/255.0f);
				buffer.put(a/255.0f);
			}
			buffer.flip();
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_FLOAT, buffer);
			glGenerateMipmap(GL_TEXTURE_2D);
			
			return tex;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	private float sideOfLine(float x1, float y1, float x2, float y2, float xp, float yp){
		float a = x2 - x1;
		float b = y2 - y1;
		float c = xp - x2;
		float d = yp - y2;
		return a*d-b*c;
	}
}
