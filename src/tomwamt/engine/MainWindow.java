package tomwamt.engine;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.system.MemoryUtil.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWFramebufferSizeCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWvidmode;
import org.lwjgl.opengl.GL;

public class MainWindow {
	private GLFWErrorCallback errorCallback;
	private GLFWFramebufferSizeCallback resizeCallback;
	private GLFWKeyCallback keyCallback;
	private GLFWMouseButtonCallback mouseCallback;
	private GLFWCursorPosCallback cursorCallback;
	private FrameCallback frameCallback;
	
	private long window;
	
	private int windowWidth;
	private int windowHeight;
	
	private int mouseX;
	private int mouseY;
	private float mouseWorldX;
	private float mouseWorldY;
	
	private int program;
	
	private int spriteVAO;
	
	private Matrix4 proj;
	private Matrix4 view;
	private Matrix4 model;
	private Camera camera;
	
	private ArrayList<Sprite> sprites;
	
	private double lastTime;
	private double deltaTime = 0;
	
	private final String projName = "proj";
	private final String viewName = "view";
	private final String modelName = "model";
	private final String samplerName = "texSampler";
	
	private static final float degToRad = (float)Math.PI/180;
	
	public MainWindow(String title, int width, int height){
		windowWidth = width;
		windowHeight = height;
		proj = new Matrix4();
		view = new Matrix4();
		model = new Matrix4();
		camera = new Camera();
		sprites = new ArrayList<Sprite>();
		
		glfwSetup(title);
		initGL();
	}
	
	public void run(){
		while(glfwWindowShouldClose(window) == GL_FALSE){
			glfwPollEvents();
			if(frameCallback != null)
				frameCallback.update((float) deltaTime);
			
			renderGL();
			
			glfwSwapBuffers(window);
			
			double currentTime = glfwGetTime();
			deltaTime = currentTime - lastTime;
			lastTime = currentTime;
		}
		
		glfwDestroyWindow(window);
		glfwTerminate();
	}
	
	public Camera getCamera(){
		return camera;
	}
	
	public void setInputCallback(InputCallback inputcb){
		if(inputcb == null){
			glfwSetKeyCallback(window, null);
			glfwSetMouseButtonCallback(window, null);
		}else{
			if(keyCallback != null){
				keyCallback.release();
			}
			keyCallback = new GLFWKeyCallback(){
				@Override
				public void invoke(long window, int key, int scancode, int action, int mods) {
					if(action == GLFW_RELEASE)
						inputcb.keyReleased(key, mods);
					else
						inputcb.keyPressed(key, mods);
				}
			};
			glfwSetKeyCallback(window, keyCallback);
			
			if(mouseCallback != null){
				mouseCallback.release();
			}
			mouseCallback = new GLFWMouseButtonCallback(){
				@Override
				public void invoke(long window, int button, int action, int mods) {
					if(action == GLFW_RELEASE)
						inputcb.mouseButtonReleased(button);
					else
						inputcb.mouseButtonPressed(button);
				}
			};
			glfwSetMouseButtonCallback(window, mouseCallback);
		}
	}
	
	public void setFrameCallback(FrameCallback framecb){
		frameCallback = framecb;
	}
	
	public void addSprite(Sprite sprite){
		sprites.add(sprite);
	}
	
	public void removeSprite(Sprite sprite){
		sprites.remove(sprite);
	}
	
	public boolean keyHeld(int key){
		return glfwGetKey(window, key) == GLFW_PRESS;
	}
	
	public boolean mouseButtonHeld(int button){
		return glfwGetMouseButton(window, button) == GLFW_PRESS;
	}
	
	public int getMouseX(){
		return mouseX;
	}
	
	public int getMouseY(){
		return mouseY;
	}
	
	public float getMouseWorldX(){
		return mouseWorldX;
	}
	
	public float getMouseWorldY(){
		return mouseWorldY;
	}
	
	public void setMouseGrabbed(boolean grab){
		// TODO
	}
	
	private void glfwSetup(String title){
		errorCallback = new GLFWErrorCallback(){
			@Override
			public void invoke(int error, long description) {
				System.err.println("[GLFW ERROR] " + error + ": " + memDecodeUTF8(description));
			}
		};
		glfwSetErrorCallback(errorCallback);
		
		if ( glfwInit() != GL_TRUE )
			throw new IllegalStateException("Unable to initialize GLFW");
		
		lastTime = glfwGetTime();
		
		glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
		glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		//glfwWindowHint(GLFW_SAMPLES, 4);
		
		window = glfwCreateWindow(windowWidth, windowHeight, title, NULL, NULL);
		if ( window == NULL )
			throw new RuntimeException("Failed to create the GLFW window");
		
		ByteBuffer vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		glfwSetWindowPos(
				window,
				(GLFWvidmode.width(vidmode) - windowWidth) / 2,
				(GLFWvidmode.height(vidmode) - windowHeight) / 2
				);
		
		resizeCallback = new GLFWFramebufferSizeCallback(){
			@Override
			public void invoke(long window, int width, int height) {
				resizeGL(width, height);
			}
		};
		glfwSetFramebufferSizeCallback(window, resizeCallback);
		
		cursorCallback = new GLFWCursorPosCallback(){
			@Override
			public void invoke(long window, double xpos, double ypos) {
				mouseX = (int) xpos;
				mouseY = (int) ypos;
				float upp = camera.getSize()/windowHeight;
				float offx = ((float)xpos - windowWidth/2)*upp;
				float offy = ((float)ypos - windowHeight/2)*upp;
				float cos = (float)Math.cos(-camera.getRot()*degToRad);
				float sin = (float)Math.sin(-camera.getRot()*degToRad);
				mouseWorldX = camera.getX() + offx*cos - offy*sin;
				mouseWorldY = camera.getY() - offy*cos - offx*sin;
			}
		};
		glfwSetCursorPosCallback(window, cursorCallback);
		
		glfwMakeContextCurrent(window);
		glfwSwapInterval(1); // v-sync
		GL.createCapabilities();
		glfwShowWindow(window);
	}
	
	private void initGL(){
		glClearColor(0, 0, 0, 0);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		program = loadShaders("glsl/vertex.glsl", "glsl/fragment.glsl");
		
		float[] vertices = {
				-0.5f, -0.5f,
				0.5f, -0.5f,
				-0.5f, 0.5f,
				
				-0.5f, 0.5f,
				0.5f, -0.5f,
				0.5f, 0.5f
		};
		
		float[] uvs = {
				0, 1,
				1, 1,
				0, 0,
				
				0, 0,
				1, 1,
				1, 0
		};
		
		spriteVAO = glGenVertexArrays();
		glBindVertexArray(spriteVAO);
		
		int vertexBuffer = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, makeFloatBuffer(vertices), GL_STATIC_DRAW);
		int positionIndex = glGetAttribLocation(program, "vertexPosition");
		glEnableVertexAttribArray(positionIndex);
		glVertexAttribPointer(positionIndex, 2, GL_FLOAT, false, 0, 0);
		
		int uvBuffer = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
		glBufferData(GL_ARRAY_BUFFER, makeFloatBuffer(uvs), GL_STATIC_DRAW);
		int uvIndex = glGetAttribLocation(program, "vertexUV");
		glEnableVertexAttribArray(uvIndex);
		glVertexAttribPointer(uvIndex, 2, GL_FLOAT, false, 0, 0);
	}
	
	private void resizeGL(int w, int h){
		glViewport(0,0,w,h);
		
		windowWidth = w;
		windowHeight = h;
	}
	
	private void renderGL(){
		glClear(GL_COLOR_BUFFER_BIT);
		
		updateProjectionMatrix();
		updateViewMatrix();
		
		glUseProgram(program);
		updateProjectionMatrix();
		int projUniform = glGetUniformLocation(program, projName);
		glUniformMatrix4fv(projUniform, false, proj.asFloatBuffer());
		int viewUniform = glGetUniformLocation(program, viewName);
		glUniformMatrix4fv(viewUniform, false, view.asFloatBuffer());
		int texUniform = glGetUniformLocation(program, samplerName);
		glUniform1i(texUniform, 0);
		int modelUniform = glGetUniformLocation(program, modelName);
		
		glBindVertexArray(spriteVAO);
		glActiveTexture(GL_TEXTURE0);
		for(Sprite sprite : sprites){
			updateModelMatrix(sprite);
			glUniformMatrix4fv(modelUniform, false, model.asFloatBuffer());
			
			glBindTexture(GL_TEXTURE_2D, sprite.getTextureID());
			glDrawArrays(GL_TRIANGLES, 0, 6);
		}
	}
	
	private void updateProjectionMatrix(){
		proj.setIdentity();
		float ar = (float)windowWidth/windowHeight;
		proj.set(0, 0, 2.0f/(ar*camera.getSize()));
		proj.set(1, 1, 2.0f/camera.getSize());
	}
	
	private void updateViewMatrix(){
		view.setIdentity();
		double t = camera.getRot() * degToRad;
		float cos = (float) Math.cos(t);
		float sin = (float) Math.sin(t);
		view.set(0, 0, cos);
		view.set(0, 1, -sin);
		view.set(1, 0, sin);
		view.set(1, 1, cos);
		view.set(3, 0, -camera.getX()*cos - camera.getY()*sin);
		view.set(3, 1,  camera.getX()*sin - camera.getY()*cos);
	}
	
	private void updateModelMatrix(Sprite sprite){
		double t = sprite.getRotation() * degToRad;
		float cos = (float) Math.cos(t);
		float sin = (float) Math.sin(t);
		
		model.setIdentity();
		model.set(0, 0, sprite.getWidth() * cos);
		model.set(0, 1, sprite.getWidth() * sin);
		model.set(1, 0, sprite.getHeight() * -sin);
		model.set(1, 1, sprite.getHeight() * cos);
		model.set(3, 0, sprite.getX());
		model.set(3, 1, sprite.getY());
	}
	
	private int loadShaders(String vertexPath, String fragmentPath){
		// Create the shaders
		int vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
		int fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

		StringBuilder vertexShaderCode = new StringBuilder();
		StringBuilder fragmentShaderCode = new StringBuilder();
		try{
			// Read the Vertex Shader code from the file
			BufferedReader inread = new BufferedReader(new FileReader(vertexPath));
			String line = inread.readLine();
			while(line != null){
				vertexShaderCode.append(line);
				vertexShaderCode.append('\n');
				line = inread.readLine();
			}
			inread.close();

			// Read the Fragment Shader code from the file
			inread = new BufferedReader(new FileReader(fragmentPath));
			line = inread.readLine();
			while(line != null){
				fragmentShaderCode.append(line);
				fragmentShaderCode.append('\n');
				line = inread.readLine();
			}
			inread.close();
		}catch(IOException ex){
			ex.printStackTrace();
			System.exit(2);
		}

		// Compile Vertex Shader
		glShaderSource(vertexShaderID, vertexShaderCode);
		glCompileShader(vertexShaderID);

		// Check Vertex Shader
		if (glGetShaderi(vertexShaderID, GL_COMPILE_STATUS) == GL_FALSE) {
			int len = glGetShaderi(vertexShaderID, GL_INFO_LOG_LENGTH);
			String error = glGetShaderInfoLog(vertexShaderID, len);
			System.err.println("Could not compile shader: "+vertexPath+"\n"+error);
			System.exit(1);
		}

		// Compile Fragment Shader
		glShaderSource(fragmentShaderID, fragmentShaderCode);
		glCompileShader(fragmentShaderID);

		// Check Fragment Shader
		if(glGetShaderi(fragmentShaderID, GL_COMPILE_STATUS) == GL_FALSE){
			int len = glGetShaderi(fragmentShaderID, GL_INFO_LOG_LENGTH);
			String error = glGetShaderInfoLog(fragmentShaderID, len);
			System.err.println("Could not compile shader: "+fragmentPath+"\n"+error);
			System.exit(1);
		}

		// Link the program
		int programID = glCreateProgram();
		glAttachShader(programID, vertexShaderID);
		glAttachShader(programID, fragmentShaderID);
		glLinkProgram(programID);

		// Check the program
		if(glGetProgrami(programID, GL_LINK_STATUS) == GL_FALSE){
			int len = glGetProgrami(programID, GL_INFO_LOG_LENGTH);
			String error = glGetProgramInfoLog(programID, len);
			System.err.println("Error linking program:\n"+error);
			System.exit(1);
		}

		glDeleteShader(vertexShaderID);
		glDeleteShader(fragmentShaderID);

		return programID;
	}
	
	private FloatBuffer makeFloatBuffer(float... floats) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(floats.length);
		buffer.put(floats);
		buffer.flip();
		return buffer;
	}
}
