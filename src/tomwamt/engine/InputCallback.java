package tomwamt.engine;

public abstract class InputCallback{
	private MainWindow window;
	
	public InputCallback(MainWindow window){
		this.window = window;
	}
	
	public MainWindow getWindow(){
		return window;
	}
	
	public abstract void keyPressed(int key, int mods);
	public abstract void keyReleased(int key, int mods);
	public abstract void mouseButtonPressed(int button);
	public abstract void mouseButtonReleased(int button);
}
