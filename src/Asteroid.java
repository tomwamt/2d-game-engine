import tomwamt.engine.Sprite;

public class Asteroid extends Sprite {
	private float speedX;
	private float speedY;
	private float speedR;
	
	public Asteroid(float speedX, float speedY, float speedR){
		super("asteroid.png");
		this.speedX = speedX;
		this.speedY = speedY;
	}
	
	public float getSpeedX(){
		return speedX;
	}
	
	public float getSpeedY(){
		return speedY;
	}
	
	public float getSpeedR(){
		return speedR;
	}
}
