import tomwamt.engine.InputCallback;
import tomwamt.engine.MainWindow;

public class ICallback extends InputCallback {
	private GameMain game;

	public ICallback(MainWindow window, GameMain game) {
		super(window);
		this.game = game;
	}

	@Override
	public void keyPressed(int key, int mods) {
		
	}

	@Override
	public void keyReleased(int key, int mods) {

	}

	@Override
	public void mouseButtonPressed(int button) {
		if(button == 0 && game.isRunning()){
			game.fireLaser();
		}
	}

	@Override
	public void mouseButtonReleased(int button) {

	}
}
