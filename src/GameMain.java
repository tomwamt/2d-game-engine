
import java.util.ArrayList;
import java.util.Random;

import tomwamt.engine.MainWindow;
import tomwamt.engine.Sprite;

public class GameMain {
	private Sprite ship;
	private ArrayList<Sprite> lasers;
	private ArrayList<Sprite> lasersToDestroy;
	private ArrayList<Asteroid> asteroids;
	private ArrayList<Sprite> asteroidsToDestroy;
	private MainWindow window;
	private boolean running = true;
	
	public Random random;
	
	public static void main(String[] args){
		GameMain game = new GameMain();
		game.lasers = new ArrayList<Sprite>();
		game.lasersToDestroy = new ArrayList<Sprite>();
		game.asteroids = new ArrayList<Asteroid>();
		game.asteroidsToDestroy = new ArrayList<Sprite>();
		game.random = new Random();
		
		MainWindow win = new MainWindow("Shmup", 1280, 720);
		game.window = win;
		
		win.getCamera().setX(3);
		win.getCamera().setSize(4);
		
		win.setFrameCallback(new FCallback(win, game));
		win.setInputCallback(new ICallback(win, game));
		
		game.ship = new Sprite("ship.png");
		game.ship.setRotation(-90);
		game.ship.setScale(0.5677f, 0.5f);
		win.addSprite(game.ship);
		
		win.run();
	}
	
	public boolean isRunning(){
		return running;
	}
	
	public void fireLaser(){
		Sprite laser = new Sprite("laser.png");
		laser.setPosition(0, ship.getY());
		laser.setScale(0.2f, 0.2f);
		laser.setRotation(-90);
		lasers.add(laser);
		window.addSprite(laser);
	}
	
	public void setShipLocation(float y){
		ship.setPosition(0, y);
	}
	
	public void updateLasers(float delta){
		float speed = 5;
		for(Sprite laser : lasers){
			float x = laser.getX()+speed*delta;
			float y = laser.getY();
			if(x > 8)
				lasersToDestroy.add(laser);
			else
				laser.setPosition(x, y);
		}
		for(Sprite laser : lasersToDestroy){
			lasers.remove(laser);
			window.removeSprite(laser);
		}
		lasersToDestroy.clear();
	}
	
	public void spawnAsteroid(){
		Asteroid asteroid = new Asteroid(random.nextFloat() + 0.5f, random.nextFloat()*0.5f - 0.25f, random.nextFloat()*20 - 10);
		asteroid.setPosition(10, random.nextFloat()*2 - 1);
		asteroid.setRotation(random.nextFloat()*360);
		asteroid.setScale(random.nextFloat()*0.5f+0.25f, random.nextFloat()*0.5f+0.25f);
		asteroids.add(asteroid);
		window.addSprite(asteroid);
	}
	
	public void updateAsteroids(float delta){
		for(Asteroid asteroid : asteroids){
			float x = asteroid.getX() - asteroid.getSpeedX()*delta;
			float y = asteroid.getY() + asteroid.getSpeedY()*delta;
			float r = asteroid.getRotation() + asteroid.getSpeedR()*delta;
			if(x < -2)
				asteroidsToDestroy.add(asteroid);
			else{
				asteroid.setPosition(x, y);
				asteroid.setRotation(r);
			}
			
			for(Sprite laser : lasers){
				if(laser.collidesWith(asteroid)){
					asteroidsToDestroy.add(asteroid);
					lasersToDestroy.add(laser);
				}
			}
			
			if(running && asteroid.collidesWith(ship)){
				running = false;
				window.removeSprite(ship);
			}
		}
		for(Sprite asteroid : asteroidsToDestroy){
			asteroids.remove(asteroid);
			window.removeSprite(asteroid);
		}
		asteroidsToDestroy.clear();
	}
}
