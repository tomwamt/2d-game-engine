import tomwamt.engine.FrameCallback;
import tomwamt.engine.MainWindow;

public class FCallback extends FrameCallback{
	private GameMain game;
	
	public FCallback(MainWindow window, GameMain game) {
		super(window);
		this.game = game;
	}

	@Override
	public void update(float delta) {
		if(game.isRunning())
			game.setShipLocation(getWindow().getMouseWorldY());
		game.updateLasers(delta);
		game.updateAsteroids(delta);
		if(game.random.nextFloat() < 0.02)
			game.spawnAsteroid();
	}
}
